package s09;

// ------------------------------------------------------------ 
public class StringSearching {
  // ------------------------------------------------------------ 
  static /*final*/ int HASHER = 301; // Maybe also try with 7 and 46237
  static /*final*/ int BASE   = 256; // Please also try with 257
  // ---------------------
  static int firstFootprint(String s, int len) {
    int result=0;
    for (int i=0; i<len; i++){
      result+= (int) s.charAt(i)*(Math.pow(BASE,s.length()-i-1));
    }
    return result;
  }
  public static int calculateHash(int print){
    return print%HASHER;
  }
  public int calculateNextSequence(int sequence, int beginning, int len, int next){
    return BASE*(sequence-beginning*len)+next;
  }
  // ---------------------
  // must absolutely be O(1)
  // coef is (BASE  power  P.LENGTH-1)  mod  HASHER
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    return BASE*(
            (previousFootprint-dropChar*coef<0?
                    dropChar*coef-previousFootprint:previousFootprint-dropChar*coef)
    )+newChar;
  }
    // h = previousFootprint
    // h = h - ...               // dropChar        (bien réfléchir !)
    // h = h * ...               // shift
    // h = h + ...               // newChar
  // ---------------------
  // Rabin-Karp algorithm
  public static int indexOf_rk(String t, String p) {
    //Obtenir les valeurs des chaînes
    int firstPrint=firstFootprint(t,p.length());
    int pFootPrint=firstFootprint(p,p.length());
    int nextPrint;
    char dropChar,newChar;
    int coef;
    //calculer le coefficient
    coef= (int) ((Math.pow(BASE,p.length()-1))%HASHER);
    //initialiser nextPrint avec la valeur de firstPrint pour aller dans la boucle
    nextPrint=firstPrint;
    for (int i=0; i<t.length();i++){
      //vérifier si les empruntes sont les mêmes avec la fonction hash
      if (calculateHash(nextPrint)==calculateHash(pFootPrint)){

        if (nextPrint==pFootPrint){
          //si les deux chaînes ont la même valeur alors elles sont les mêmes
          return i;
        }
      }
      dropChar=t.charAt(t.length()-i-1);
      if (p.length()+i>=t.length()){
        break;
      }
      newChar=t.charAt(p.length()+i);
      nextPrint=nextFootprint(nextPrint,dropChar,newChar,coef);
    }
    return -1;
  }
}
