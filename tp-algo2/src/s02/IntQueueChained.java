package s02;

import java.util.Arrays;
import java.util.Random;

public class IntQueueChained {
  //======================================================================
  /* TODO: adapt using pseudo-pointers instead of queue node objects
   * "Memory management" code:
   * - define "memory" arrays, the NIL constant, and firstFreeCell
   * - define allocate/deallocate, with automatic array expansion
   * "User" code:
   * - modify enqueue/dequeue/..., keeping the same logic/algorithm
   * - test
   */
  //======================================================================

  //======================================================================
  private int front=0;
  private int back=0;
  private int[] elts=new int[10];
  private int[] nexts={1,2,3,4,5,6,7,8,9,-1};
  private final int NIL=-1;
  private static int firstFreeCell=0;
  // ------------------------------
  public IntQueueChained() {

  }
  // --------------------------
  public void enqueue (int elt) {
    if (back==0){
      back=allocate();
      front=back;
    }else {
      back=allocate();
    }
    elts[back]=elt;
  }

  public int allocate(){
    if (firstFreeCell==NIL){
      doubleSize();
    }
    int occupiedIndex=firstFreeCell;
    firstFreeCell=nexts[occupiedIndex];
    nexts[occupiedIndex]=NIL;

    return occupiedIndex;
  }
  public void deallocate(int i){
    nexts[i]=firstFreeCell;
    firstFreeCell=i;
  }


  public void doubleSize(){
    firstFreeCell=nexts.length;
    nexts=Arrays.copyOf(nexts,nexts.length*2);
    elts=Arrays.copyOf(elts,elts.length*2);
  }
  // --------------------------
  public boolean isEmpty() {
    return Arrays.equals(elts,new int[elts.length]);
  }
  // --------------------------
  public int consult() {
    return elts[front];
  }
  // --------------------------
  public int dequeue() {
      int e=elts[front];
      deallocate(front);
      return e;
  }
}
