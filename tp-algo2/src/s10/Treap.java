package s10;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>>  {
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static Random rnd=new Random(); 
    // -----------------------
    private final E elt;
    private int     pty;
    // -----------------------
    public TreapElt(E e) {
      elt=e; 
      pty=rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o==null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt==null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return ""+elt+"#"+pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }
  //============================================================
  private final BST<TreapElt<E>> bst;
  // --------------------------
  public Treap() {
    bst=new BST<TreapElt<E>>();
  }

  public void add(E e) {
    //Guarantees that the element is not already there
    if (!contains(e)){
      BTreeItr<TreapElt<E>> itr =new BTreeItr<TreapElt<E>>(bst.tree);
      TreapElt<E> elt= new TreapElt<>(e);
      //Go to root
      while (!itr.isRoot()){
        itr=itr.up();
      }
      //Compare until bottom is reached
      while (!itr.isBottom()){
        if (itr.consult().compareTo(elt)>0){
          itr=itr.right();
        }else{
          itr=itr.left();
        }
      }
      //Insert element at bottom
      itr.insert(elt);

      //place by priority
      while (true){
        //if the root is reached then the up method can't be called anymore
        if (itr.isRoot()){
          break;
        }
        /*if the priority of the recent element is bigger than its parent's
        * rotate left if the value is bigger and rotate right if the value is lesser
        * until the priority of the added element is lesser than its parent's.*/
        if(itr.consult().pty()<itr.up().consult().pty()){
          if (compareParentAndChild(itr)>0){
            itr.rotateRight();
          }else if (compareParentAndChild(itr)<0){
            itr.rotateLeft();
          }else{
            //This ensures that the loop will break eventually
            break;
          }
        }else{
          //The right spot that respects the priority has been found
          break;
        }
      }

    }
  }
  /*This method returns 1 if the child is bigger, 0 if it's the same and -1 if it's lesser*/
  public int compareParentAndChild(BTreeItr<TreapElt<E>> itr){
    return itr.consult().compareTo(itr.up().consult());
  }

  public void remove(E e) {
    if(contains(e)){
      bst.remove(new TreapElt<E>(e));
    }
  }

  public boolean contains(E e) {
    boolean result=false;
    BTreeItr<TreapElt<E>> itr =new BTreeItr<TreapElt<E>>(bst.tree);
    TreapElt<E> elt= new TreapElt<>(e);
    //Go to root
    while (!itr.isRoot()){
      itr=itr.up();
    }
    //Compare until bottom is reached
    while (!itr.isBottom()){
      if (itr.consult().compareTo(elt)>0){
        itr=itr.right();
      }else if(itr.consult().compareTo(elt)<0){
        itr=itr.left();
      }else {
        //If compareTo returns 0 then the value is the same
        result=true;
        break;
      }
    }
    /*
    * Alternatively it could return bst.contains(elt)*/
    return result;
  }

  public int size() {
    return bst.size();
  }

  public E minElt() {
    //It goes to the leftmost and returns the value
    return bst.minElt().elt();
  }

  public E maxElt() {
    //It goes to the rightmost and returns the value
    return bst.maxElt().elt();
  }
  
  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    BTreeItr<TreapElt<E>> smallestElt;
    BTreeItr<TreapElt<E>> rightElt=ti.right();
    BTreeItr<TreapElt<E>> leftElt=ti.left();
    //find smallest
    if (rightElt.consult().compareTo(leftElt.consult())<0){
      smallestElt=rightElt;
    }else {
      smallestElt=leftElt;
    }
    //if the smallest is bigger then it swaps with its parent and it cuts
    if (smallestElt.consult().compareTo(ti.consult())<0){
      ti.rotateRight();
      siftDownAndCut(new BTreeItr<TreapElt<E>>(smallestElt.cut()));
    }

  }

  private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      if (ti.isLeftArc()) {ti=ti.up(); ti.rotateRight();}
      else                {ti=ti.up(); ti.rotateLeft(); }
    }
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca= a.consult();
    TreapElt<E> cb= b.consult();
    return ca.pty()<cb.pty();
  }
}
