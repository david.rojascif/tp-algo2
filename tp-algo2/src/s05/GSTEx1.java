package s05;
public class GSTEx1 {
  // ------------------------------------------------------------
  public static void main(String [] args) {
    int [] c = {+1, +2, +3, +4, +5, +6, +7, +8, +9, -1, -2, -3, -4, -5, -6};
    GeneralSearchTreeMap<Integer, Integer> t;
    t = new GeneralSearchTreeMap<Integer, Integer>(3);
    for(int i:c) {
      System.out.println("-------------------------  "+i +" :");
      if (i<0) t.remove(-i);
      else     t.put   ( i, i);
      System.out.println(t);
    }
    /*
    showArray(c);
    rotate(c,1);
    showArray(c);*/
  }
  public static  void showArray(int [] c){
    String result="[";
    for (int i=0; i<c.length;i++) {
      if (i == c.length - 1) {
        result=result.concat(c[i] + "]");
      } else {
        result=result.concat(c[i] + ",");
      }
    }
    System.out.println(result);
  }
  public static void rotate(int[] t, int rotation){
    //méthode pour effectuer une rotation à gauche
    int numberOfElementsToPlace=t.length;
    int[] indexAndValue=new int[2];
    indexAndValue[0]=numberOfElementsToPlace-1;//index
    indexAndValue[1]=t[numberOfElementsToPlace-1];//value
    while (numberOfElementsToPlace>=0){//boucle pour placer chaque élément à sa place
      place(t,rotation,indexAndValue);
      numberOfElementsToPlace--;
    }
  }
  public static void place(int[] t, int rotation,int[] indexAndValue){
    //cette méthode sert à placer le couple indexe-élément du tableau à la bonne place
    int sub, newIndex, currentIndex;
    currentIndex=indexAndValue[0];
    newIndex=currentIndex-rotation;
    if (newIndex<0){
      newIndex+=t.length;
    }
    sub=t[newIndex];
    t[newIndex]=indexAndValue[1];
    indexAndValue[0]=newIndex;
    indexAndValue[1]=sub;
  }
}
