package s07;

import java.util.Arrays;
// ------------------------------------------------------------
public class DisjointSets {
  int[] elements;//le tableau d'éléments
  int nbElements;//la valeur qui compte le nombre d'éléments
  public DisjointSets(int nbOfElements) {
    elements= new int[nbOfElements];
    for (int i=0; i<elements.length; i++) {
      elements[i]=-1;
    }
    this.nbElements=nbOfElements;
  }

  public boolean isInSame(int i, int j) {
    if (i==j) return true;// est-ce que les deux sont la même valeur? Si oui alors ça s'arrête là
    boolean itIsInSame= false;
    while (i>=0){//la boucle itère sur les parents de i
      if (elements[i]==j){//est-ce que j est un des parents de i?
        itIsInSame=true;// si oui alors isInSame est true, si non alors false
        break;
      }else {
        i=elements[i];
      }
    }
    return itIsInSame;
  }


  public void union(int i, int j) {
    if (i==j) return;
    i=root(i);
    int elementsOfI=elements[i];
    int elementsOfJ=elements[root(j)];
    elements[i]=j;
    elements[j]=elementsOfI+elementsOfJ;
  }
  private int root(int elt){
    if (elements[elt]<0){
      return elt;
    }
    return root(elements[elt]);
  }


  public int nbOfElements() {  // as given in the constructor
    return nbElements;
  }
  
  public int minInSame(int i) {
    while (true){
      if (i>elements[i]){// itérer jusqu'à trouver plus petit parent
        i=elements[i];
      }else{
        break;
      }
    }
    return i;
  }
  //si tous les éléments sont dans un même ensemble alors
  //le root correspondra au nombre d'éléments.
  public boolean isUnique() {
    return elements[root(0)]*-1==this.nbElements;
  }

  @Override
  public String toString() {
    String result="";
    for (int iterator=0; iterator<elements.length; iterator++){
      if (result.lastIndexOf(elements[iterator])<0){//if element is not in result, add it
        result=result.concat(setToString(elements[iterator])+",");
      }
    }
    StringBuilder pseudoResult= new StringBuilder(result);
    pseudoResult.deleteCharAt(result.lastIndexOf(','));

    return pseudoResult.toString();
  }
  //creates a string representing the set of the element
  private String setToString(int elt){
    String result="{";
    for (int i=0; i<elements.length; i++){
      if (isInSame(elt,elements[i])){
          result=result.concat(elements[i]+",");
      }
    }
    StringBuilder pseudoResult= new StringBuilder(result);
    pseudoResult.setCharAt(result.lastIndexOf(','),'}');

    return pseudoResult.toString();
  }


  @Override
  public boolean equals(Object otherDisjSets) {
    if (otherDisjSets instanceof DisjointSets){
      DisjointSets other=(DisjointSets)otherDisjSets;
      if (other.nbOfElements()!=this.nbOfElements()){
        return false;
      }
      for (int i=0; i<other.nbOfElements();i++){
        if (other.elements[i]!=this.elements[i]){
          return false;
        }
      }
      return true;
    }else {
      return false;
    }
  }
}
